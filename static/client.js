

var socket = io.connect(window.location.origin);

var inGame = false;

// generate an initial angle between -60 degrees and +60 degrees:
var initAngle = -60 + 120*Math.random();
 
// randomly choose the initial direction of the ball:
var initDirection = Math.random() < 0.5 ? -1 : 1;


var startGame = function (){

    //show the game canvas:
    document.getElementById("gameContainer").style.display = "block";
     
    //initialize the game canvas:
    pong.init();
     
    //move the ball to the center:
    pong.resetBall();

};

var lobby= [];

var currentuser = "";
var player_score = 0;
var opponent_score = 0;

window.addEventListener("paddlehit-left", function(e){
    // e.detail.hit will be true if the client hit the ball with his/her paddle.
    if (e.detail.hit) {
        console.log("HIT PADDLE.  New angle: %f", e.detail.angle); // log a message to the console
 
        // Tell our opponent via Socket.IO about the ball's new angle and position:
        socket.emit("reflect", {
            angle: e.detail.angle,
            position: e.detail.position
        });
 
        // Note: The game automatically launches the ball and determines the angle based on the ball's position relative to the paddle position.  We therefore do not need to call pong.launch() in here (but our opponent will, as shown below).
    }else{
        opponent_score+=1;
        score(player_score,opponent_score);
        if (opponent_score > 2){
            socket.emit("gameOver");
            alert("You Lose!");
            
            sendname(lobby[currentuser]);
            document.getElementById("gameContainer").style.display ="none";
        }
        else {
            var angle = -60 + 120*Math.random();
            var initDirection = Math.random() < 0.5 ? -1 : 1;
            pong.resetBall();
            pong.launch(angle, initDirection);
            socket.emit("initiateGame", { initial: [initAngle, initDirection] });
            pauseGame();
        }
    }
});

socket.on("setid", function(data){
    currentuser = data;
});

socket.on("gameOver", function(data){
    
     
   
    alert("You win!");
  

    sendname(lobby[currentuser]);
    document.getElementById("gameContainer").style.display ="none";

});



// Listen for the "reflect" message from the server
socket.on("reflect", function(data){
    // In order to make up for any network lag, immediately move the ball to its correct position:
    pong.resetBall(960, data.position);
 
    // Finally, launch the ball on our local game client:
    data.angle  = data.angle *-1;
    pong.launch(data.angle, -1);
    
    
});

socket.on("launch", function(data){
    $('#myModal').modal('hide');
    document.getElementById("modal_player_picker").innerHTML = "";

    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='beginGame()'>Start Game</button>";
    console.log("startGame");
    startGame();

    
});

var beginGame = function(opponent_id){
    
    socket.emit("startgame", { initial: [initAngle, initDirection] });
    
    // socket.emit("challenge", { current_id: currentuser, opponent_id: opponent_id });

};



socket.on("startgame", function(data){
    alert("Ready to Start?");
    
    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='pauseGame()'><i class='fa fa-pause'></i></button>";


    pong.launch(initAngle, initDirection);
    score(0,0);
    socket.emit("initiateGame", { initial: [initAngle, initDirection] });
    // initial = data.initial
    // pong.launch(initial[0],initial[1]*-1);
    
});


socket.on("initiateGame", function(data){
    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='pauseGame()'><i class='fa fa-pause'></i></button>";
    
    initial = data.initial;
    pong.launch(initial[0],initial[1]*-1);
    
});




var score = function(x,y){

    console.log("SCORE SHOULD BE "+ x + " AND "+ y);
    player_score = x;
    opponent_score = y;

    pong.setScore({left:x,right:y});
    socket.emit("score", {player_score:x,opponent_score:y});

};

socket.on("score", function(data){

    player_score = data.player_score;
    opponent_score = data.opponent_score;
    
    console.log("SCORE SHOULD BE "+ player_score + " AND "+ opponent_score);
    pong.setScore({left:player_score,right:opponent_score});
    
});


var pauseGame = function(){
    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='unpauseGame()'><i class='fa fa-play'></i></button>";
    pong.pauseOrResumeGame();
    socket.emit("pause");

};
var unpauseGame = function(){
    
    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='pauseGame()'><i class='fa fa-pause'></i></button>";
    socket.emit("unpause");
    pong.pauseOrResumeGame();

};


socket.on("pause", function(data){
    pong.pauseOrResumeGame();

    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='unpauseGame()'><i class='fa fa-play'></i></button>";   
});

socket.on("unpause", function(data){
    
    document.getElementById("pregame").innerHTML = "<button class='btn btn-default' onclick='pauseGame()'><i class='fa fa-pause'></i></button>";  
    pong.pauseOrResumeGame();
   

     
});



var sendname = function(name) {
    if (typeof name === 'undefined'){
        name = escape(document.getElementById("nickname").value);

    }
    socket.emit("nickname", { name: name });

};

socket.on("lobby", function(data){
    //array of sockets
    lobby = data;
    document.getElementById("pregame").innerHTML = "";
    document.getElementById("modal_player_picker").innerHTML = "";


    document.getElementById("modal_player_picker_header").innerHTML = lobby[currentuser];


    document.getElementById("modal_player_picker").innerHTML += "<table class='table table-hover' id='game-lobby' ></table>";
    
    for (var player in lobby){
        console.log(player);
        console.log(currentuser);

        if (player != currentuser){
            document.getElementById("game-lobby").innerHTML +="<tr><td>" + lobby[player] + "</td><td><button class='btn btn-default pull-right' id='"+ player + "' onclick='sendplayertoserver(this.id)'>Play</button></td></tr>";
            
        }
        
    }
    $('#myModal').modal('show');
});


var sendplayertoserver = function(opponent_id){

    
    socket.emit("challenge", { current_id: currentuser, opponent_id: opponent_id });

};


window.addEventListener("paddlemove", function(e){
   socket.emit("paddlemove", { position: e.detail.position });
});



socket.on("confirm", function(data){
    console.log(data);
    var opponent_id = data.opponent_id;
    var current_id = data.current_id;


    console.log( "curent user is right" + data.current_id == currentuser);



    var tooPlay = confirm(lobby[opponent_id] + " would like to play with you");
    if (tooPlay)
      {
        //Correct ID 
        socket.emit("confirm", { current_id: current_id, opponent_id: opponent_id });
      }
    else
      {
        //Correct ID
        socket.emit("reject", { current_id: current_id, opponent_id: opponent_id });
      }
});




socket.on("paddlemove", function(data){
    pong.updateOpponentPaddle(data.position);
});