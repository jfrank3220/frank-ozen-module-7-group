var http = require('http'),
    url = require('url'),
    path = require('path'),
    mime = require('mime'),
    path = require('path'),
    fs = require('fs'),
    io = require("socket.io");

// Make a simple fileserver for all of our static content.
// Everything underneath <STATIC DIRECTORY NAME> will be served.
var app = http.createServer(function (req, resp) {
    var filename = path.join(__dirname, "/static", url.parse(req.url).pathname);
    (fs.exists || path.exists)(filename, function (exists) {
        if (exists) {
            fs.readFile(filename, function (err, data) {
                if (err) {
                    // File exists but is not readable (permissions issue?)
                    resp.writeHead(500, {
                        "Content-Type": "text/plain"
                    });
                    resp.write("Internal server error: could not read file");
                    resp.end();
                    return;
                }

                // File exists and is readable
                var mimetype = mime.lookup(filename);
                resp.writeHead(200, {
                    "Content-Type": mimetype
                });
                resp.write(data);
                resp.end();
                return;
            });
        } else {
            // File does not exist
            resp.writeHead(404, {
                "Content-Type": "text/plain"
            });
            resp.write("Requested file not found: " + filename);
            resp.end();
            return;
        }
    });
});

app.listen(3456);

io = io.listen(app);

io.sockets.on("connection", function (socket) {
    // This closure runs when a new Socket.IO connection is established.

    function getSocketById(id) {
    var clients = io.sockets.clients(),
        client = null;
    clients.forEach(function (_client) {
        if (_client.id === id) 
        {
            client = _client;
        }return client;
    });
    return client;
    }

    function updateLobby(){
        var unpaired = {};
        
        io.sockets.clients().forEach(function(client){
            if (client.paired === false) {
                unpaired[client.id] = client.name;
            }
        });
        io.sockets.clients().forEach(function(client){
            
            if (client.paired === false) {
                client.emit("setid", client.id);
                client.emit("lobby", unpaired);

            }
        });
    }

    //add it to the list of clients
    socket.on("nickname", function (data) {
        // Set socket name to the nickname
        socket.name = data.name;


        // Add socket to arry of unpaired sockets
        socket.paired = false;

        // Update lobbies of all sockets
        updateLobby();
    });
    socket.on("pause", function(data){
        socket.opponent.emit("pause");
    });

    socket.on("unpause", function(data){
        socket.opponent.emit("unpause");   
    });

    socket.on("initiateGame", function(data){
        socket.opponent.emit("initiateGame", data);   
    });

    socket.on("challenge", function (data) {
        // Get socket to pair with
       
        current_id = data.current_id;
        opponent_id = data.opponent_id;
        var req = getSocketById(opponent_id);

        // Emit confirm message to opponent socket
        console.log("tedt");
        req.emit("confirm", { current_id: data.opponent_id, opponent_id: data.current_id });
    });

    socket.on("confirm", function (data) {
        // Get opponent socket
        // console.log(data)
        // console.log(data.socket_id)
        current_id = data.current_id;
        opponent_id = data.opponent_id;

        var player1 = getSocketById(current_id);
        var player2 = getSocketById(opponent_id);

        // Set sockets opponents to each other
        player1.opponent = getSocketById(opponent_id);
        player2.opponent = getSocketById(current_id);

        player1.paired = true;
        player2.paired = true;
        // Update lobby
        updateLobby();

        // Tell both players to launch game
        player1.emit("launch");
        player2.emit("launch");
    });

    socket.on("reject", function (data) {
        var req = getSocketById(data.socket_id);

        // Inform challenger that their challenge was rejected
        req.emit("reject");
    });


    socket.on("score", function(data){
        var player_score = data.opponent_score;
        var opponent_score = data.player_score;
        console.log("SCORE SHOULD BE "+ player_score + " AND "+ opponent_score);
        socket.opponent.emit("score",{player_score:player_score, opponent_score:opponent_score});
    });

    socket.on("startgame", function(data){
        socket.opponent.emit("startgame", data);
    });

    

    socket.on("paddlemove", function (data) {
        socket.opponent.emit("paddlemove", data);
    });

    socket.on("reflect", function (data) {
        socket.opponent.emit("reflect", data);
    });

    socket.on("gameOver", function(data){
        socket.opponent.emit("gameOver");
    });
});